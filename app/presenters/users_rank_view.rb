class UsersRankView
  attr_reader :users

  def initialize(context, user)
    @context = context
    @user = user
    @users = User::UserQuery.new.ordered_by_rank
  end

  def table_headers
    users_headers + games_headers
  end

  def table_footers
    Array.new(table_headers.size).tap do |footers|
      footers[0] = "Players: #{@users.count}"
      footers[1] = "Games: #{Game.count}"
    end
  end

  def user_names(user)
    if @user.from_usa?
      [user.last_name, user.first_name]
    else
      [user.first_name, user.last_name]
    end
  end

  def user_row_class(user)
    if rank_leader?(user)
      'winner'
    elsif rank_top?(user)
      'top10'
    elsif rank_bottom?(user)
      'bottom3'
    end
  end

  def user_header_class(user)
    'border' if user == @user
  end

  def games_stats(user)
    games_count = user.games.count.to_f
    [
        user.wins.count,
        user.draws.count,
        user.loses.count,
        NumberFormatter.to_percentage((user.wins.count / games_count) * 100),
        NumberFormatter.to_percentage((user.draws.count / games_count) * 100),
        NumberFormatter.to_percentage((user.loses.count / games_count) * 100),
    ]
  end

  private

  def users_headers
    if @user.from_usa?
      ['Last name', 'First name']
    else
      ['First name', 'Last name']
    end
  end

  def games_headers
    ['Score', 'Wins', 'Draws', 'Loses', '% of wins', '% of draws', '% of loses']
  end

  def rank_leader?(user)
    @users.first == user
  end

  def rank_top?(user, number = 10)
    @users.index(user) < number
  end

  def rank_bottom?(user, number = 3)
    @users.index(user) > @users.count - number - 1
  end
end