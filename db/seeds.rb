# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

20.times do |n|
  User.create first_name: "John", last_name: "Doe #{n}"
end

User.all.each do |home_player|
  User.all.each do |away_player|
    unless home_player == away_player
      home_score = rand(5)
      away_score = rand(3)
      Game.create home_score: home_score, away_score: away_score, home_player: home_player, away_player: away_player
    end
  end
end